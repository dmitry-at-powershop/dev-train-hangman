require_relative 'hangman.rb'
require_relative 'console_view.rb'
require_relative 'console_read.rb'

# Controller for Hangman game
class Controller
  def initialize(game: HangmanGame.new, view: ConsoleView.new, input: ConsoleRead.new)
    @game = game
    @view = view
    @input = input
  end

  def play
    while @game.playing?
      @view.show @game.state
      @game.add_letter @input.read
    end
    @view.show @game.state
  end
end
