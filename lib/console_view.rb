# Printing the state of the Hangman game to console
class ConsoleView
  MASK_CHAR = '_'.freeze

  def show(state)
    @state = state
    system 'clear'
    print_header
    print_attempts_left
    print_masked_word
    print_tried_letters
    print_error
    print_result
    print_prompt
  end

  def print_header
    puts 'Hangman game'
  end

  def print_masked_word
    puts "\n", masked_word(@state.word, @state.letters)
  end

  def print_attempts_left
    puts "#{@state.attempts_left} attempts left"
  end

  def print_tried_letters
    puts "Letters tried so far: #{@state.letters}"
  end

  def print_error
    puts "Error! #{@state.input_error}" if @state.input_error
  end

  def print_result
    puts "You have #{@state.result}!" if @state.result
  end

  def print_prompt
    puts 'Enter a letter:' if @state.keep_playing
  end

  def masked_word(word, letters)
    word.split('').reduce('') do |masked, letter|
      masked += letters.include?(letter) ? letter.upcase : MASK_CHAR
    end
  end
end

#   +--+
#   |  |
#   O  |
#  /|\ |
#  / \ |
#      |
# =======
