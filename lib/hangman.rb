require 'set'
require_relative 'hangman_state'

# Hangman game
class HangmanGame
  DEFAULT_WORD = 'powershop'.freeze
  DEFAULT_ATTEMPTS = 5

  def initialize(word: DEFAULT_WORD, attempts: DEFAULT_ATTEMPTS)
    @secret_word = word.downcase
    @attempts_left = attempts
    @letters = Set.new
    @input_error = nil
    validate_game
  end

  def playing?
    !result
  end

  def state # { word: 'shop', attempts_left: 5, letters: [], result=>'lost' }
    current_state = HangmanState.new
    current_state.word = @secret_word
    current_state.attempts_left = @attempts_left
    current_state.letters = @letters.to_a.sort
    current_state.result = result
    current_state.keep_playing = playing?
    current_state.input_error = @input_error
    current_state
  end

  def add_letter(input)
    input.chomp!
    input.downcase!
    @input_error = input_error(input)
    return if @input_error || !playing?
    @letters.add(input)
    @attempts_left -= 1 unless @secret_word.include? input
  end

  private

  def result
    return 'lost' if @attempts_left <= 0
    return 'won' if @letters >= @secret_word.chars.to_set
  end

  def validate_game
    raise ArgumentError, 'Secret word is not a single word with latin letters!' unless @secret_word =~ /\A[a-z]+\z/
    raise ArgumentError, 'Number of attempts should be integer number' unless @attempts_left.is_a? Integer
    raise ArgumentError, 'Number of attempts should be positive!' unless @attempts_left > 0
  end

  def input_error(input)
    return 'A single letter is expected' unless input && input.length == 1
    return 'A latin letter is expected' unless input =~ /[a-z]/
    return 'You have already tried this letter' if @letters.include?(input)
  end
end
