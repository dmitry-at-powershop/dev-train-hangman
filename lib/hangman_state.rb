# Transfers the game state to view
HangmanState = Struct.new(
  :word,
  :attempts_left,
  :letters,
  :result,
  :keep_playing,
  :input_error
)
