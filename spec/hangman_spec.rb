require 'simplecov'
SimpleCov.start

require 'hangman'

describe HangmanGame do
  describe '.initialize' do
    context 'given no arguments' do
      let(:game) { HangmanGame.new }
      it 'sets default word and default number of attempts' do
        expect(game.instance_variable_get(:@secret_word)).to eql(HangmanGame::DEFAULT_WORD)
        expect(game.instance_variable_get(:@attempts_left)).to eql(HangmanGame::DEFAULT_ATTEMPTS)
      end

      context 'given a word and a number of attempts' do
        let(:game) { HangmanGame.new(word: 'secretword', attempts: 100) }
        it 'sets the word and the number of attempts' do
          expect(game.instance_variable_get(:@secret_word)).to eql('secretword')
          expect(game.instance_variable_get(:@attempts_left)).to eql(100)
        end
      end
    end
  end

  describe '.validate_game' do
    let(:game) { HangmanGame.new(word: 'X', attempts: 1) }

    context 'given proper word and positive number of attempts' do
      it 'returns nothing' do
        expect(game.send(:validate_game)).to eql(nil)
      end
    end

    context 'given an empty word' do
      before { game.instance_variable_set(:@secret_word, '') }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end

    context 'given two words' do
      before { game.instance_variable_set(:@secret_word, 'hello there') }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end

    context 'given a word with a non letter character' do
      before { game.instance_variable_set(:@secret_word, 'powersh0p') }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end

    context 'given a word with a trailing whitespace' do
      before { game.instance_variable_set(:@secret_word, "powershop\n") }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end

    context 'given a word with a non letter character' do
      before { game.instance_variable_set(:@secret_word, 'powersh0p') }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end
    context 'given zero attmpts' do
      before { game.instance_variable_set(:@attempts_left, 0) }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end

    context 'given zero attmpts' do
      before { game.instance_variable_set(:@attempts_left, -3.14) }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end

    context 'given a string a number of attempts' do
      before { game.instance_variable_set(:@attempts_left, '0') }
      it 'raises an ArgumentError' do
        expect { game.send(:validate_game) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '.playing?' do
    let(:game) { HangmanGame.new(word: 'xyz', attempts: 3) }

    context 'with no letters guessed and initial number of attempts' do
      it 'returns false' do
        expect(game.playing?).to eql(true)
      end
    end

    context 'with some letters guessed and positive number of attempts' do
      before do
        %w(a b x).map { |x| game.add_letter(x) }
      end
      it 'returns false' do
        expect(game.playing?).to eql(true)
      end
    end

    context 'with all letters guessed (and some attempts left)' do
      before do
        %w(a b x y z).map { |x| game.add_letter(x) }
      end
      it 'returns false' do
        expect(game.playing?).to eql(false)
      end
    end

    context 'with some letters guessed and 0 attempts left' do
      before do
        %w(a b c d e f x z).map { |x| game.add_letter(x) }
      end
      it 'returns false' do
        expect(game.playing?).to eql(false)
      end
    end
  end

  describe '.state' do
    let(:game) { HangmanGame.new(word: 'xyz', attempts: 3) }
    context 'when the game is still in progress' do
      before do
        %w(a b x y 0).map { |x| game.add_letter(x) }
      end
      it 'returns the proper state' do
        expect(game.state.word).to eql('xyz')
        expect(game.state.letters).to eql(%w(a b x y))
        expect(game.state.input_error).to eql('A latin letter is expected')
        expect(game.state.keep_playing).to eql(true)
      end
    end
    context 'when the game is over' do
      before do
        %w(a b x y z).map { |x| game.add_letter(x) }
      end
      it 'returns the proper state' do
        expect(game.state.word).to eql('xyz')
        expect(game.state.letters).to eql(%w(a b x y z))
        expect(game.state.result).to eql('won')
        expect(game.state.keep_playing).to eql(false)
      end
    end
  end
end
