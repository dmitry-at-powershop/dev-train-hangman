require 'simplecov'
SimpleCov.start

require 'console_view'
require 'hangman_state'

describe ConsoleView do
  before { $stdout = StringIO.new }
  after(:all) { $stdout = STDOUT }

  describe '.show' do
    context 'being called with a starting state' do
      before do
        state = HangmanState.new
        state.word = 'shop'
        state.attempts_left = 5
        state.letters = []
        state.keep_playing = true
        ConsoleView.new.show(state)
      end
      it 'prints header' do
        expect($stdout.string).to match(/^Hangman game$/)
      end
      it 'prints a fully masked word' do
        expect($stdout.string).to match(/^#{ConsoleView::MASK_CHAR * 4}$/)
      end
      it 'prints an empty list of tried letters' do
        expect($stdout.string).to match(/^Letters tried so far: \[\]$/)
      end
      it 'prints a number of attempts left' do
        expect($stdout.string).to match(/^5 attempts left$/)
      end
      it 'prints a prompt' do
        expect($stdout.string).to match(/^Enter a letter:$/)
      end
    end
    context 'being called with some letters guessed and an error' do
      before do
        state = HangmanState.new
        state.word = 'shop'
        state.attempts_left = 7
        state.letters = %w(a b h o)
        state.input_error = 'Bad input'
        state.keep_playing = true
        ConsoleView.new.show(state)
      end
      it 'prints a partially masked word' do
        expect($stdout.string).to match(/^#{ConsoleView::MASK_CHAR}HO#{ConsoleView::MASK_CHAR}$/)
      end
      it 'print an updated number of attempts left' do
        expect($stdout.string).to match(/^7 attempts left$/)
      end
      it 'prints a sorted set of tried letters' do
        expect($stdout.string).to match(/^Letters tried so far: \["a", "b", "h", "o"\]$/)
      end
      it 'prints the error' do
        expect($stdout.string).to match(/^Error! Bad input$/)
      end
    end
    context 'being called with a game result' do
      before do
        state = HangmanState.new
        state.word = 'shop'
        state.attempts_left = 0
        state.letters = %w(a b h o)
        state.result = 'lost'
        state.keep_playing = false
        ConsoleView.new.show(state)
      end
      it 'prints the result' do
        expect($stdout.string).to match(/^You have lost!$/)
        expect($stdout.string).not_to match(/Enter a letter:/)
      end
    end
  end
end
