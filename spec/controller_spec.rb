require 'simplecov'
SimpleCov.start

require 'controller'
require 'console_helpers'

describe Controller do
  describe '.play' do
    context 'given a word and a good sequence of input' do
      before do
        winning_typer = Autotyper.new %w(a b c p o w e r s h o p)
        listener = Autoreader.new
        Controller.new(input: winning_typer, view: listener).play
        @final_state = listener.state(-1)
      end
      it 'prints the won state' do
        expect(@final_state['word']).to eql('powershop')
        expect(@final_state['result']).to eql('won')
        expect(@final_state['attempts_left']).to eql(2)
        expect(@final_state['letters']).to eql(%w(a b c p o w e r s h o p).sort.uniq)
      end
    end
    context 'given a word and a good sequence of input' do
      before do
        loosing_typer = Autotyper.new %w(a b c o w e r s h o d e f)
        listener = Autoreader.new
        Controller.new(input: loosing_typer, view: listener).play
        @final_state = listener.state(-1)
      end
      it 'prints the won state' do
        expect(@final_state['word']).to eql('powershop')
        expect(@final_state['result']).to eql('lost')
        expect(@final_state['attempts_left']).to eql(0)
        expect(@final_state['letters']).to eql(%w(a b c o w e r s h o d e f).sort.uniq)
      end
    end
  end
end
