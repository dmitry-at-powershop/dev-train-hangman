# Mocks user typing letters
class Autotyper
  def initialize(input)
    @input = input
  end

  def read
    @input.shift
  end
end

# Saves data printed by the game
class Autoreader
  def initialize
    @states = []
  end

  def show(state)
    @states.push(state)
  end

  def state(step)
    @states[step]
  end
end
